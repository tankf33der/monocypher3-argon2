(load "monocypher.l")
(load "libargon2i.l")
(test
   (libargon2i 3 8
      (mapcar char (chop "mikemike"))
      (mapcar char (chop "mikemike"))
      8 )
   (crypto_argon2i 3 8
      (mapcar char (chop "mikemike"))
      (mapcar char (chop "mikemike"))
      8 ) )
(msg 'ok)
(bye)
