(de libargon2i (Iters Blocks Pass Salt HashLen)
   (let
      (PL (length Pass)
         SL (length Salt)
         R NIL )
      (native
         "libargon2.so"
         "argon2i_hash_raw"
         'I
         Iters
         Blocks
         1
         (cons NIL (cons PL) Pass)
         PL
         (cons NIL (cons SL) Salt)
         SL
         (list 'R (cons HashLen 'B HashLen))
         HashLen )
      R ) )
